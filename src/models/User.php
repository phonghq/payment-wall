<?php


class User
{
    private $conn;
    private $table = 'users';

    public $id;
    public $username;
    public $name;
    public $email;
    public $password;
    public $status;
    public $country;
    public $currency;
    public $created_at;

    public function __construct(
        $db) {
        $this->conn = $db;
    }

    public function read() {
        // Create query
        $query = 'SELECT username, email, status, country, currency FROM ' . $this->table . ' ORDER BY created_at DESC';
        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    public function read_single(){
        // Create query
        $query = 'SELECT
          id,
          username,
          status,
          country,
          currency,
        FROM
          ' . $this->table . '
      WHERE id = ?
      LIMIT 0,1';

        //Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set properties
        $this->id = $row['id'];
        $this->name = $row['name'];
    }

    public function create() {
        // Create Query
        $query = 'INSERT INTO ' .
            $this->table . '
    SET
      name = :name';

        // Prepare Statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->name = htmlspecialchars(strip_tags($this->name));

        // Bind data
        $stmt-> bindParam(':name', $this->name);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: \n", $stmt->error);

        return false;
    }

    public function update() {
        // Create Query
        $query = 'UPDATE ' .
            $this->table . '
    SET
      name = :name
      WHERE
      id = :id';

        // Prepare Statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind data
        $stmt-> bindParam(':name', $this->name);
        $stmt-> bindParam(':id', $this->id);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: .\n", $stmt->error);

        return false;
    }

    // Delete Category
    public function delete() {
        // Create query
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

        // Prepare Statement
        $stmt = $this->conn->prepare($query);

        // clean data
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind Data
        $stmt-> bindParam(':id', $this->id);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: .\n", $stmt->error);

        return false;
    }
}