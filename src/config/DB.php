<?php


class DB
{
    public $conn;
    protected $host = "localhost";
    protected $username = "root";
    protected $password = "1234567";
    protected $db_name = "payment_wall";

    public function connection(){
        $this->conn = null;
        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
        }catch (Exception $e){
            echo 'Connection is error |'.$e->getMessage();
            print_r( $e );
        }
    }
}