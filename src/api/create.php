<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once "./src/config/DB.php";
include_once "./src/models/User.php";
// Instantiate DB & connect
$database = new DB();
$db = $database->connection();

// Instantiate blog post object
$user = new User($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$user->name = $data->name;

// Create Category
if($user->create()) {
    echo json_encode(
        array('message' => 'Users Created')
    );
} else {
    echo json_encode(
        array('message' => 'User is not Created')
    );
}