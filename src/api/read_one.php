<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../config/DB.php';
include_once '../models/User.php';
// Instantiate DB & connect
$database = new DB();
$db = $database->connection();

$user = new User($db);

// Get ID
$user->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get post
$user->read_single();

// Create array
$userData = array(
    'id' => $userModel->id,
    'name' => $userModel->name
);

// Make JSON
print_r(json_encode($userData));