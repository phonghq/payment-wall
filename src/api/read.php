<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


include_once "./src/config/DB.php";
include_once "./src/models/User.php";
// Instantiate DB & connect
$database = new DB();
$db = $database->connection();

$userModel = new User($db);

// Category read query
$result = $userModel->read();

// Get row count
$num = $result->rowCount();

// Check if any categories
if($num > 0) {
    // Cat array
    $cat_arr = array();
    $cat_arr['data'] = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $user = array(
            'id' => $id,
            'name' => $name
        );

        // Push to "data"
        array_push($cat_arr['data'], $user);
    }

    // Turn to JSON & output
    echo json_encode($cat_arr);

} else {
    // No Categories
    echo json_encode(
        array('message' => 'No Categories Found')
    );
}